import { Controller, Get, Query } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('users')
export class UserController {

    constructor(
        private userService: UserService
    ) {}

    @Get()
    all(@Query() query) {
        return this.userService.all(query)
    }
}
