import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserModule } from './user/user.module';
import { ProductModule } from './product/product.module';
import { BoughtHistoryModule } from './bought_history/bought_history.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './shared/logging.interceptor';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'test',
            password: '12345',
            database: 'test',
            logging: false,
            entities: ['./**/*.entity.js'],
            synchronize: true,
            acquireTimeout: 100000
        }),
        UserModule,
        ProductModule,
        BoughtHistoryModule,
    ],
    controllers: [],
    providers: [{
        provide: APP_INTERCEPTOR,
        useClass: LoggingInterceptor
    }],
})
export class AppModule { }
