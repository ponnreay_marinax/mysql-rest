

import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BoughtHistoryEntity } from "./bought_history.entity";
import { BoughtHistoryService } from "./bought_history.service";
import { BoughtHistoryController } from "./bought_history.controller";


@Module({
    imports: [ TypeOrmModule.forFeature([BoughtHistoryEntity])],
    providers: [BoughtHistoryService],
    controllers: [BoughtHistoryController],
})
export class BoughtHistoryModule {}