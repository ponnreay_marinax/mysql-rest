import { Controller, Get } from "@nestjs/common";
import { BoughtHistoryService } from "./bought_history.service";




@Controller('bought_histories')
export class BoughtHistoryController {

    constructor(
        private boughtHistoryService: BoughtHistoryService
    ) {}

    @Get()
    all() {
        return this.boughtHistoryService.all()
    }
}