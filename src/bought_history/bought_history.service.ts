import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { BoughtHistoryEntity } from "./bought_history.entity";
import { Repository } from "typeorm";

@Injectable()
export class BoughtHistoryService {

    constructor(
        @InjectRepository(BoughtHistoryEntity) private boughtHistoryRepo: Repository<BoughtHistoryEntity>
    ) {}

    async all() {
        return this.boughtHistoryRepo.find({
            relations: ['product', 'user']
        })
    }

    create(data: Partial<BoughtHistoryEntity>) {
        const entity = this.boughtHistoryRepo.create(data)
        return this.boughtHistoryRepo.save(entity)
    }
}