import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { UserEntity } from "../user/user.entity";
import { ProductEntity } from "../product/product.entity";


@Entity('bought_histories')
export class BoughtHistoryEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({type: 'int'})
    product_id: number

    @Column({type: 'int'})
    user_id: number

    @Column('datetime')
    date: string

    @ManyToOne(type => UserEntity, user => user.bought_histories)
    @JoinColumn({
        name: 'user_id'
    })
    user: UserEntity

    @ManyToOne(type => ProductEntity, product => product.bought_histories)
    @JoinColumn({
        name: 'product_id'
    })
    product: ProductEntity
}