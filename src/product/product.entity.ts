import { PrimaryGeneratedColumn, Column, Entity, OneToOne, JoinColumn, OneToMany } from "typeorm";
import { BoughtHistoryEntity } from "../bought_history/bought_history.entity";

@Entity('products')
export class ProductEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({
        type: 'varchar',
        length: 255
    })
    name: string

    @Column({
        type: 'decimal',
        precision: 13,
        scale: 4
    })
    price: number

    @OneToMany(type => BoughtHistoryEntity, bought_history => bought_history.product)
    @JoinColumn({
        name: 'id'
    })
    bought_histories: BoughtHistoryEntity[]
}