import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

const port = 3000
const host = 'localhost'

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {cors: true});
    await app.listen(port, host);
    Logger.log(`Server running on http://${host}:${port}`, 'Boostrap')
}
bootstrap();
